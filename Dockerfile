FROM python:3.8.5

RUN python -V

RUN python -m pip install --upgrade pip

WORKDIR /code

ENV PYTHONUNBUFFERED=1

COPY requirements.txt /code/

RUN pip install -r requirements.txt
COPY . /code/

# Creates a non-root user and adds permission to access the /app folder
# RUN useradd appuser && chown -R appuser /code
# USER appuser
