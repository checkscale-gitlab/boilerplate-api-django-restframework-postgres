#!/bin/bash

docker-compose run api python ./manage.py makemigrations
docker-compose run api python ./manage.py migrate --noinput
docker-compose run api python ./manage.py create_superuser
docker-compose run api python ./manage.py create_public_user_group
docker-compose run api python manage.py test