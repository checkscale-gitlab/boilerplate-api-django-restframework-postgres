import { request } from 'src/http/client';

/**
 * Register a new user
 * @param email
 * @param password
 * @returns Promise<void>
 */
export const signup = (email: string, password: string) => {
  return request('/user/register', {
    data: {
      email: email,
      password: password,
    },
    method: 'POST',
  });
};

/**
 * Login a user
 * @param email
 * @param password
 * @returns Promise
 */
export const login = (email: string, password: string) => {
  return request<undefined | { email: string[] }>('/user/login', {
    data: {
      email: email,
      password: password,
    },
    method: 'POST',
  });
};

/**
 * Logout a user
 * @returns void
 */
export const logout = () => {
  return request('/user/logout', {
    method: 'GET',
  });
};

/**
 * Get a user session
 * @returns object
 */
export const session = () => {
  return request<{ isAuthenticated: boolean }>('/user/session', {
    method: 'GET',
  });
};

/**
 * Get csrf token
 * @returns object
 */
export const getCsrf = () => {
  return request('/user/csrf', {
    method: 'GET',
  });
};
