import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

import store, { RootState } from 'src/state';

let csrf: string | null = '';

const updateCsrf = () => {
  const state: RootState = store.getState();
  csrf = state.session.csrf;
};
store.subscribe(updateCsrf);

export function httpClient() {
  const instance = axios.create({
    baseURL: 'http://localhost:8000/api/v1',
    timeout: 10000,
    xsrfHeaderName: 'X-CSRFToken',
    withCredentials: true,
  });

  return instance;
}

export function buildParams(
  url: string,
  params: AxiosRequestConfig | undefined,
) {
  const paramsBase: AxiosRequestConfig = {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'X-CSRFToken': csrf,
    },
    method: 'GET',
    data: null,
    url,
    withCredentials: true,
  };
  return Object.assign({}, paramsBase, params);
}

export function request<T>(
  url: string,
  params?: AxiosRequestConfig,
): Promise<AxiosResponse<T>> {
  const config = buildParams(url, params);
  const client = httpClient();
  return client.request(config);
}
