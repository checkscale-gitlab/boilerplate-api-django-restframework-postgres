import React from 'react';

import { Link } from 'react-router-dom';

import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import { Formik, FormikProps } from 'formik';
import * as Yup from 'yup';

import { useAuth } from 'src/contexts/auth-context';

const validationSchema = Yup.object({
  email: Yup.string()
    .email('Enter a valid email')
    .required('Email is required'),
  password: Yup.string().required('Enter a valid password'),
});

export default function SignupPage() {
  const { signup, setSignupStep } = useAuth();

  React.useEffect(() => {
    setSignupStep(0);
  }, []);

  const handleSubmit = (email: string, password: string) => {
    signup(email, password);
  };

  const initialValues = {
    email: '',
    password: '',
  };

  return (
    <>
      <Paper style={{ padding: 20 }}>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={(values: { email: string; password: string }) =>
            handleSubmit(values.email, values.password)
          }
        >
          {(formProps: FormikProps<{ email: string; password: string }>) => (
            <SignupForm {...formProps} />
          )}
        </Formik>
      </Paper>
    </>
  );
}

function SignupForm(props: FormikProps<{ email: string; password: string }>) {
  const { loading, loginErrors, signupStep } = useAuth();

  const {
    values: { email, password },
    errors,
    handleSubmit,
    handleChange,
    isValid,
  } = props;

  if (signupStep > 0) {
    return (
      <div style={{ paddingTop: 20, paddingBottom: 20 }}>
        <Typography variant="subtitle1" color="inherit">
          An email was sent to ...
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          Confirm your email and then login.
        </Typography>
        <Typography variant="subtitle1" color="inherit">
          <Link to="/login">Login page</Link>
        </Typography>
      </div>
    );
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Signup</h2>
      {loginErrors.message ? (
        <Typography variant="caption" color="error">
          {loginErrors.message}
        </Typography>
      ) : null}
      <TextField
        name="email"
        autoComplete="on"
        type="email"
        disabled={loading}
        helperText={errors.email}
        error={Boolean(errors.email)}
        label="Email"
        fullWidth
        value={email}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />
      <TextField
        name="password"
        autoComplete="on"
        type="password"
        disabled={loading}
        helperText={errors.password}
        error={Boolean(errors.password)}
        label="Password"
        fullWidth
        value={password}
        onChange={handleChange}
        style={{ marginTop: 20 }}
        variant="outlined"
      />

      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={!isValid || loading}
        style={{ marginTop: 40 }}
        startIcon={
          loading ? <CircularProgress size={24} color="inherit" /> : null
        }
        fullWidth
      >
        Signup
      </Button>
    </form>
  );
}
