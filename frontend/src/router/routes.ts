import LoginPage from '../pages/login';
import LandingPage from '../pages/landing';
import NotFoundPage from '../pages/404';
import DashboardPage from '../pages/dashboard';

export const routes = [
  {
    path: '/',
    exact: true,
    component: LandingPage,
  },
  {
    path: '/login',
    component: LoginPage,
  },
  {
    path: '/dashboard',
    component: DashboardPage,
  },
  {
    path: '*',
    component: NotFoundPage,
  },
];
