import { UserActionTypes, SET_CSRF_TOKEN } from './types';

export function setCSRF(csrf: string | null): UserActionTypes {
  return {
    type: SET_CSRF_TOKEN,
    payload: {
      csrf: csrf,
    },
  };
}
