import { UserActionTypes, SET_CSRF_TOKEN } from './types';

const initialState: { csrf: string | null } = {
  csrf: null,
};

export function userReducer(state = initialState, action: UserActionTypes) {
  switch (action.type) {
    case SET_CSRF_TOKEN:
      return {
        ...state,
        csrf: action.payload.csrf,
      };
    default:
      return state;
  }
}
