export interface IReactChildren {
  children: JSX.Element;
}
