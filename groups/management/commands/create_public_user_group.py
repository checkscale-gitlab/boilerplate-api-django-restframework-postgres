from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group, Permission


class Command(BaseCommand):
    """
    Create PublicUserGroup
    """

    def handle(self, *args, **options):

        # create todos perm group
        group, created = Group.objects.get_or_create(name="PublicUserGroup")

        if created:
            print("Creating public user group...")
            c = Permission.objects.get(codename="add_todo")
            r = Permission.objects.get(codename="view_todo")
            u = Permission.objects.get(codename="change_todo")
            d = Permission.objects.get(codename="delete_todo")

            group.permissions.add(c, r, u, d)
            print("PublicUserGroup created and permissions added...")
        else:
            print("PublicUserGroup exists, skipping...")
