import uuid

from django.db import models


class Todo(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    description = models.TextField()
    owner = models.ForeignKey(
        "user.User", related_name="todos", on_delete=models.CASCADE, default=None
    )
