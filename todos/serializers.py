from rest_framework import serializers
from todos.models import Todo


class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ["id", "created_at", "updated_at", "uid", "description"]

    def save(self, **kwargs):
        # fields
        user = self.context["request"].user

        new_todo = Todo(description=self.validated_data["description"], owner=user)

        new_todo.save()

        return new_todo
