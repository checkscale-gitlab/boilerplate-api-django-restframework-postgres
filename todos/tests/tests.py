from user.models import User
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient, APITestCase

from todos.models import Todo


class GetSingleTodoTest(APITestCase):
    """
    Test module for GET a todo API
    """

    def setUp(self):
        """
        Test setup
        """

        # Create the client to make requests
        self.client = APIClient()

        # create a test user
        self.user = User(
            username="test@test.com",
        )
        self.user.set_password("SomethingHard2Gue$$")
        self.user.save()

        self.client.login(username="test@test.com", password="SomethingHard2Gue$$")

    def test_create_todo(self):
        """
        Test create a todo
        """
        todo_description = "test we can create a todo"
        url = reverse("todo-list")
        data = {"description": todo_description}

        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Todo.objects.count(), 1)
        self.assertEqual(Todo.objects.get().description, todo_description)

    def test_get_todo_list(self):
        """
        Test GET all todos
        """

        # ##################################
        # create some todos
        #
        # ##################################

        for x in range(3):
            todo = Todo(owner=self.user, description=f"test todo {x}")
            todo.save()

        # get the list of todos
        response = self.client.get(reverse("todo-list"))

        # test response status
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # test todos created
        todos = Todo.objects.all()

        self.assertEqual(todos.count(), 3)

    def test_get_todo_item(self):
        """
        Test GET todo detail
        """

        # ##################################
        # create one todo and fetch it by id
        #
        # ##################################
        todo = Todo(owner=self.user, description="test todo 1")
        todo.save()

        url = reverse("todo-detail", kwargs={"pk": todo.pk})

        # get the list of todos
        response = self.client.get(url)

        # test response status
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_todo_owners(self):

        # ##################################
        # first create a few users and
        # todos for each user
        #
        # ##################################

        for x in range(2):

            creds = {
                "email": f"test{x}@test.com",
                "password": "SomethingHard2Gue$$",
                "confirm_password": "SomethingHard2Gue$$",
            }

            url = "/api/v1/user/register"

            # create new user
            response = self.client.post(url, creds, format="json")

            # get newly created user
            user = User.objects.filter(email=f"test{x}@test.com").first()

            # get registration token assigned to user
            token = user.registration_token

            # user clicks confirm link with token
            url = f"/user/register/confirm?token={token}"
            response = self.client.get(url)

            # get updated user
            user = User.objects.filter(email=f"test{x}@test.com").first()

            for x in range(2):
                todo = Todo(owner=user, description=f"test todo {x}")
                todo.save()

        # ##################################
        # Now test that users can only
        # view todos that they own/created
        #
        # ##################################

        # get test0@test.com user
        user = User.objects.filter(email="test0@test.com").first()

        # logout setup user
        self.client.logout()

        # login as test1@test.com
        self.client.login(username="test1@test.com", password="SomethingHard2Gue$$")

        # get the list of todos
        response = self.client.get(reverse("todo-list"))

        # ##################################
        # Asserstions
        #
        # ##################################

        self.assertEqual(response.data["count"], 2)

        # TODO: test against model since 'owner' was removed from api response
        # for todo in response.data["results"]:
        #     # test that there are not todos returned
        #     # to test1@test.com user that belong to
        #     # test0@test.com user
        #     self.assertNotEqual(todo["owner"], user.id)
