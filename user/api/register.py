from rest_framework.response import Response
from rest_framework import generics, status

from ..serializers.registration import RegistrationSerializer


class PostRegisterNewUser(generics.CreateAPIView):
    """
    Register a new user
    """

    serializer_class = RegistrationSerializer

    def post(self, request, format=None):
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class PostRegisterNewUser(generics.CreateAPIView):
#     """
#     Register a new user
#     """

#     serializer_class = RegistrationSerializer

#     def post(self, request, format=None):
#         serializer = RegistrationSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
