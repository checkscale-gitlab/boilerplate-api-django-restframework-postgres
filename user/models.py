import uuid

from django.db import models

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    registration_code = models.IntegerField(blank=True, null=True)
    email_verified = models.BooleanField(default=False)
    uid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    registration_token = models.CharField(
        unique=True, null=True, default=None, max_length=90
    )
    registration_token_expires = models.DateTimeField(blank=True, null=True)
    locked_out = models.BooleanField(default=False)
    locked_out_expires = models.DateTimeField(blank=True, null=True)
    login_attempts = models.IntegerField(default=0)

    recover_account_token = models.CharField(
        unique=True, null=True, default=None, max_length=90
    )
    recover_account_token_expires = models.DateTimeField(blank=True, null=True)
