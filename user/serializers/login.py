from rest_framework import serializers

from user.models import User


class LoginSerializer(serializers.ModelSerializer):
    """
    Log the user in for session
    """

    email = serializers.EmailField()
    password = serializers.CharField(max_length=500, style={"input_type": "password"})

    class Meta:
        model = User
        fields = ["email", "password"]
