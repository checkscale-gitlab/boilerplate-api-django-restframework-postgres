from rest_framework import serializers

from user.models import User


class ResetPasswordSerializer(serializers.ModelSerializer):
    """
    Serializer for reset password step 1.

    The user requests a pw recovery link w/
    their email
    """

    email = serializers.EmailField()

    class Meta:
        model = User
        fields = ["email"]


class ConfirmResetPasswordTokenSerializer(serializers.Serializer):
    """
    Confirm the reset password token
    """

    token = serializers.CharField()

    class Meta:
        fields = ["token"]
